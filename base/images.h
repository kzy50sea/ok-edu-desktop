/*
 * Copyright (c) 2022 船山科技 chuanshantech.com
 * OkEDU is licensed under Mulan PubL v2.
 * You can use this software according to the terms and conditions of the Mulan
 * PubL v2. You may obtain a copy of Mulan PubL v2 at:
 *          http://license.coscl.org.cn/MulanPubL-2.0
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PubL v2 for more details.
 */

//
// Created by gaojie on 23-9-17.
//
#pragma once

#include <QImage>

namespace base {
class Images {

public:
  inline static bool putToPixmap(const QByteArray &data,
                                           QImage &image) {
#ifdef Q_OS_WIN
    if (data.size() > 13 * 1024) {
      qWarning() << "图片大小超过13k无法渲染";
      return false;
    }
#endif
    return image.loadFromData(data);
  }
};
} // namespace base
