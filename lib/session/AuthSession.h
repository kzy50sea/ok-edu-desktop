#pragma once

#include <QObject>
#include <QThread>
#include <QTimer>
#include <memory>

#include <base/OJSON.h>
#include <base/basic_types.h>

#include "base/OkAccount.h"
#include "lib/network/NetworkHttp.h"
#include "lib/network/network.h"
#include <QUrl>
#include <utility>

namespace ok {
namespace session {

enum LOGIN_STATUS {
  NONE = 0,
  CONNECTING,
  SUCCESS,
  FAILURE,
};

class LoginResult {
public:
  LOGIN_STATUS status = NONE;
  QString msg;
  QString token;
};

class AuthInfo {
public:
  AuthInfo() = default;

  AuthInfo(AuthInfo &info) {
    token_ = info.getToken();
    clientName_ = info.getClientName();
  }

  ~AuthInfo() = default;

  const QString &getToken() const { return token_; }

  const QString &getClientName() const { return clientName_; }

  void fromJSON(const QJsonObject &data) {
    token_ = data.value("token").toString();
    clientName_ = data.value("clientName").toString();
  }

  QJsonObject toJSON() {
    QJsonObject qo;
    qo.insert("token", token_);
    qo.insert("clientName", clientName_);
    return qo;
  }

private:
  QString token_;
  QString clientName_;
};

/**
 * 登录信息
 */
struct SignInInfo {
  // 账号
  QString account;
  // 密码
  QString password;
};

class AuthSession : public QObject {
  Q_OBJECT
public:
  AuthSession(QObject *parent = nullptr);
  ~AuthSession() override;

  static AuthSession *Instance();

  bool authenticated() const;

  void doLogin(const SignInInfo &signInInfo);

  [[nodiscard]] const SignInInfo &getSignInInfo() const {
    return m_signInInfo;
  };

  [[nodiscard]] const QString &getToken() const { return token_; };

  [[nodiscard]] const AuthInfo &authInfo() const { return _authInfo; }

  [[nodiscard]] ok::base::OkAccount* account()const{
    return okAccount.get();
  }

  static LOGIN_STATUS status();

protected:
  void initTimer();

  void doConnect();

private:
  SignInInfo m_signInInfo;

  std::shared_ptr<AuthSession> _session;
  std::unique_ptr<QTimer> _timer;

  std::unique_ptr<network::NetworkHttp> m_networkManager;

  AuthInfo _authInfo;

  QString token_;

  std::mutex _mutex;

  std::unique_ptr<ok::base::OkAccount> okAccount;

signals:
  void loginResult(SignInInfo, LoginResult); // LoginResult

public slots:
  void timerUp();
};
} // namespace session
} // namespace ok