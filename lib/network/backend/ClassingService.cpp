﻿#include "ClassingService.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <memory>
#include <string>

#include "base/r.h"
#include <lib/session/AuthSession.h>

#include <base/basic_types.h>
#include <base/logs.h>

#include <lib/network/backend/BaseService.h>
#include <lib/network/backend/HttpService.h>

namespace backend {

using namespace ok::session;

ClassingService::ClassingService(QObject *parent) : BaseService(parent) {}

ClassingService::~ClassingService() {}

bool ClassingService::requestInfo(Fn<void(QJsonObject)> callback) {
  QString baseUrl = _baseUrl + "/welcome/info.do";
  QMap<QString, QString> params;
  HttpService httpService;
  return httpService.request(baseUrl, params, callback);
}

} // namespace backend
