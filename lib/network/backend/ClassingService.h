﻿#pragma once

#include <QJsonObject>
#include <QObject>
#include <QString>
#include <memory>
#include <string>

#include <base/basic_types.h>

#include <lib/network/backend/BaseService.h>
#include <lib/session/AuthSession.h>

namespace backend {

using namespace ok::session;

/*上课信息*/
class ClassingService : BaseService {
  Q_OBJECT

public:
  ClassingService(QObject *parent = nullptr);
  ~ClassingService();

  bool requestInfo(Fn<void(QJsonObject)> callback);
};
} // namespace backend
