﻿#pragma once

#include <QJsonObject>
#include <QObject>
#include <QString>

#include <base/basic_types.h>

#include <lib/network/backend/BaseService.h>
#include <lib/network/backend/HttpService.h>
#include <lib/network/backend/domain/AuthInfo.h>
#include <lib/network/backend/domain/UserInfo.h>

#include "lib/network/NetworkManager.h"
#include "lib/session/AuthSession.h"

namespace backend {

class UserService : public BaseService {
  Q_OBJECT
public:
  UserService(QObject *parent = nullptr);
  ~UserService();

  void login(const QString &username, //
             const QString &password, //
             Fn<void(const ok::session::AuthInfo &)> callback);

  void getInfo(Fn<void(const UserInfo &)> callback);

  void getByUin(const QString &uin, Fn<void(const UserInfo &)> callback);

  void setUserType(const QString &userType);
};

} // namespace backend
