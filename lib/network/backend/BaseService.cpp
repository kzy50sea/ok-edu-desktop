﻿#include "BaseService.h"

#include <QObject>
#include <QString>

#include "lib/network/NetworkHttp.h"
#include "base/r.h"

#include <algorithm>
#include <base/singleton.h>
#include <lib/session/AuthSession.h>

namespace backend {

BaseService::BaseService(QObject *parent)
    : QObject(parent),
      m_networkManager(std::make_unique<network::NetworkHttp>(this)),
      _baseUrl(BACKEND_BASE_URL) {
}

BaseService::~BaseService() {}

} // namespace backend
